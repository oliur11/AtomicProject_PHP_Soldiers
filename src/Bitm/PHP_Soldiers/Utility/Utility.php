<?php

namespace App\BITM\PHP_Soldiers\Utility;

class Utility {

    static public function d($param = false) {
        echo "<pre>";
        var_dump($param);
        echo "</pre>";
    }

    static public function dd($param = false) {
        self::d($param);
        die();
    }

    private static function setMessage($message) {
        $_SESSION['message'] = $message;
    }

    private static function getMessage() {
        if (isset($_SESSION['message'])) {
            $message = $_SESSION['message'];
            $_SESSION['message'] = NULL;
            return $message;
        } else {
            $_SESSION['message'] = NULL;
        }
    }

    public static function message($message = NULL) {
        if (is_null($message)) {
            $output = self::getMessage();
            return $output;
        } else {
            self::setMessage($message);
        }
    }

    static public function redirectHobby($url = "/AtomicProject_PHP_Soldiers/Views/PHP_Soldiers/Checkbox/index.php") {
        header("Location:" . $url);
    }

    static public function redirectBirthday($url = "/AtomicProject_PHP_Soldiers/Views/PHP_Soldiers/Date/index.php") {
        header("Location:" . $url);
    }

    static public function redirectEmail($url = "/AtomicProject_PHP_Soldiers/Views/PHP_Soldiers/Email/index.php") {
        header("Location:" . $url);
    }

    static public function redirectPicture($url = "/AtomicProject_PHP_Soldiers/Views/PHP_Soldiers/File/index.php") {
        header("Location:" . $url);
    }

    static public function redirectGender($url = "/AtomicProject_PHP_Soldiers/Views/PHP_Soldiers/Radio/index.php") {
        header("Location:" . $url);
    }

    static public function redirectCity($url = "/AtomicProject_PHP_Soldiers/Views/PHP_Soldiers/Select/index.php") {
        header("Location:" . $url);
    }

    static public function redirectBook($url = "/AtomicProject_PHP_Soldiers/Views/PHP_Soldiers/Text/index.php") {
        header("Location:" . $url);
    }

    static public function redirectSummary($url = "/AtomicProject_PHP_Soldiers/Views/PHP_Soldiers/Textarea/index.php") {
        header("Location:" . $url);
    }

    static public function redirectBookmark($url = "/AtomicProject_PHP_Soldiers/Views/PHP_Soldiers/Textareamultiple/index.php") {
        header("Location:" . $url);
    }

    static public function redirectMobile($url = "/AtomicProject_PHP_Soldiers/Views/PHP_Soldiers/Textmultiple/index.php") {
        header("Location:" . $url);
    }

}
