

<div class="navbar">
    <div class="navbar-inner">
        <div style="text-align: center; padding: 15px 0px">Design & Developed by PHP Soldiers</div>
    </div>
</div>


<!-- start: JavaScript-->

<script src="../../../Resource/bootstrap/js/jquery-1.9.1.min.js"></script>
<script src="../../../Resource/bootstrap/js/jquery-migrate-1.0.0.min.js"></script>

<script src="../../../Resource/bootstrap/js/jquery-ui-1.10.0.custom.min.js"></script>

<script src="../../../Resource/bootstrap/js/jquery.ui.touch-punch.js"></script>

<script src="../../../Resource/bootstrap/js/modernizr.js"></script>

<script src="../../../Resource/bootstrap/js/bootstrap.min.js"></script>

<script src="../../../Resource/bootstrap/js/jquery.cookie.js"></script>

<script src='../../../Resource/bootstrap/js/fullcalendar.min.js'></script>

<script src='../../../Resource/bootstrap/js/jquery.dataTables.min.js'></script>

<script src="../../../Resource/bootstrap/js/excanvas.js"></script>
<script src="../../../Resource/bootstrap/js/jquery.flot.js"></script>
<script src="../../../Resource/bootstrap/js/jquery.flot.pie.js"></script>
<script src="../../../Resource/bootstrap/js/jquery.flot.stack.js"></script>
<script src="../../../Resource/bootstrap/js/jquery.flot.resize.min.js"></script>

<script src="../../../Resource/bootstrap/js/jquery.chosen.min.js"></script>

<script src="../../../Resource/bootstrap/js/jquery.uniform.min.js"></script>

<script src="../../../Resource/bootstrap/js/jquery.cleditor.min.js"></script>

<script src="../../../Resource/bootstrap/js/jquery.noty.js"></script>

<script src="../../../Resource/bootstrap/js/jquery.elfinder.min.js"></script>

<script src="../../../Resource/bootstrap/js/jquery.raty.min.js"></script>

<script src="../../../Resource/bootstrap/js/jquery.iphone.toggle.js"></script>

<script src="../../../Resource/bootstrap/js/jquery.uploadify-3.1.min.js"></script>

<script src="../../../Resource/bootstrap/js/jquery.gritter.min.js"></script>

<script src="../../../Resource/bootstrap/js/jquery.imagesloaded.js"></script>

<script src="../../../Resource/bootstrap/js/jquery.masonry.min.js"></script>

<script src="../../../Resource/bootstrap/js/jquery.knob.modified.js"></script>

<script src="../../../Resource/bootstrap/js/jquery.sparkline.min.js"></script>

<script src="../../../Resource/bootstrap/js/counter.js"></script>

<script src="../../../Resource/bootstrap/js/retina.js"></script>

<script src="../../../Resource/bootstrap/js/custom.js"></script>
<!-- end: JavaScript-->

</body>
</html>
